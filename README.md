## Building:

### On IntelliJ:

File-> Settings->Build, Execution, Deployment -> Compiler ->
Java Compiler -> Project bytecode version: 1.8

To get a jar at $CLASSPATH/out/artifacts: Build -> Build Artifacts


## TODO:
- [] Test cancelVoucher
- [] Put some exception handling logic in there.
- [] Find a way to inject depositorConfiguration to exnCourierService
so as to simplify methods.