package com.existanze.services.libraries.couriers.dto

class VoucherInfo {

    // Default constructor should exist to support
    // e.g. populating from db scenario
    // Courier quirks should be applied here based on courierCode
    VoucherInfo(){}
    VoucherInfo(String courierCode, DepositorInfo depositorInfo, def orderReport){
        this.courierCode = courierCode
        orderCode = orderReport.code
        depositorCode = depositorInfo.depositorCode
        depositorName = depositorInfo.depositorName
        voucherCallbackUrl = depositorInfo.voucherCallbackUrl
        pdfCallbackUrl = depositorInfo.pdfCallbackUrl
        orderData = new OrderData(orderReport)
        requestor = new RequestorInfo(courierCode)
        consignee = new ConsigneeInfo(orderReport)
        payment = new PaymentInfo(orderReport)
    }
    String courierCode
    String orderCode
    String depositorCode
    String depositorName
    String voucherCallbackUrl
    String pdfCallbackUrl
    OrderData orderData

    RequestorInfo requestor
    ConsigneeInfo consignee
    PaymentInfo payment
}
