package com.existanze.services.libraries.couriers.services

import com.existanze.services.libraries.couriers.configuration.CourierConfiguration
import com.existanze.services.libraries.couriers.dto.VoucherInfo
import com.existanze.services.libraries.couriers.util.CourierUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpStatusCodeException
import org.springframework.web.client.RestTemplate

@Service("exnCourierService")
class ExnCourierService {

    Logger logger = LoggerFactory.getLogger(ExnCourierService.class)

    @Autowired
    RestTemplate courierRestTemplate

    @Autowired
    CourierConfiguration courierConfiguration


    ResponseEntity getRequest(def orderCode, def depositorCode) {
        def uriVariable = [
                'orderCode'    : orderCode,
                'depositorCode': depositorCode
        ]
        try {
            return courierRestTemplate.getForEntity(courierConfiguration.courierUrl + '/request?orderCode={orderCode}&depositorCode={depositorCode}', Map.class, uriVariable)
        } catch (HttpStatusCodeException e) {
            if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
                throw e
            }
            return ResponseEntity.status(e.getRawStatusCode()).body(e.getResponseBodyAsString())
        }
    }

    ResponseEntity requestVoucher(VoucherInfo voucherInfo) {
        def request = [
                "orderCode"         : voucherInfo.orderCode,
                "depositorCode"     : voucherInfo.depositorCode,
                "depositorName"     : voucherInfo.depositorName,
                "voucherCallbackUrl": voucherInfo.voucherCallbackUrl,
                "pdfCallbackUrl"    : voucherInfo.pdfCallbackUrl,
                "orderData"         : [
                        "notes"        : voucherInfo.orderData.notes,
                        "orderType"    : voucherInfo.orderData.orderType,
                        "serviceType"  : voucherInfo.orderData.serviceType,
                        "totalValue"   : voucherInfo.orderData.totalValue,
                        "totalQuantity": voucherInfo.orderData.totalQuantity,
                        "totalWeight"  : voucherInfo.orderData.totalWeight,
                        "items"        : []

                ],
                "requestor"         : [
                        "code"   : voucherInfo.requestor.code,
                        "name"   : voucherInfo.requestor.name,
                        "address": voucherInfo.requestor.address,
                        "area"   : voucherInfo.requestor.area,
                        "city"   : voucherInfo.requestor.city,
                        "phone"  : voucherInfo.requestor.phone,
                        "zip"    : voucherInfo.requestor.zip,
                        "email"  : voucherInfo.requestor.email,
                ],
                "consignee"         : [
                        "code"   : voucherInfo.consignee.code,
                        "name"   : voucherInfo.consignee.name,
                        "area"   : voucherInfo.consignee.area,
                        "city"   : voucherInfo.consignee.city,
                        "address": voucherInfo.consignee.address,
                        "phone"  : voucherInfo.consignee.phone,
                        "phone2" : voucherInfo.consignee.phone2,
                        "zip"    : voucherInfo.consignee.zip,
                        "email"  : voucherInfo.consignee.email,
                        "country": voucherInfo.consignee.country
                ],
                "payment"           : [
                        "method": voucherInfo.payment.method,
                        "amount": voucherInfo.payment.amount
                ]
        ]
        voucherInfo.orderData.items.each {
            voucherItem ->
                def item = [
                        "description": voucherItem.description ?: '',
                        "weight"     : voucherItem.weight ?: 1,
                        "value"      : voucherItem.value ?: 1
                ]
                request.orderData.items.add(item)
        }

        HttpHeaders headers = new HttpHeaders()
        headers.set('content-type', 'application/json')

        HttpEntity entity = new HttpEntity(request, headers)

        try {
            return courierRestTemplate.postForEntity(courierConfiguration.courierUrl + '/' + voucherInfo.courierCode + '/request/voucher', entity, Map.class)
        } catch (HttpStatusCodeException e) {
            logger.error('Error during voucher request for order {}, {}', voucherInfo.orderCode, CourierUtils.extractErrors(e))
            return ResponseEntity.status(e.getRawStatusCode()).body(CourierUtils.extractErrors(e))
        }
    }

    ResponseEntity requestPdf(String orderCode, Integer requestId, String courierCode, String depositorCode) {
        HttpHeaders headers = new HttpHeaders()
        headers.set('content-type', 'application/json;charset=utf-8')

        HttpEntity entity = new HttpEntity(['requestId': requestId, 'depositorCode': depositorCode], headers)

        try {
            return courierRestTemplate.postForEntity(courierConfiguration.courierUrl + '/' + courierCode + '/request/pdf', entity, Map.class)
        } catch (HttpStatusCodeException e) {
            logger.error('Error during pdf request for order: {} with requestId: {}, {}', orderCode, requestId, CourierUtils.extractErrors(e))
            return ResponseEntity.status(e.getRawStatusCode()).body(CourierUtils.extractErrors(e))
        }
    }

     def cancelRequest(Integer id, String courierCode, String depositorCode) {

        //Box now doesn't support cancel
        if(courierCode == '103'){
            return
        }

        def uriVariables = [
                'id'           : id,
                'depositorCode': depositorCode
        ]

        return courierRestTemplate.delete(courierConfiguration.courierUrl + '/request/{id}?depositorCode={depositorCode}', uriVariables)
    }
}
