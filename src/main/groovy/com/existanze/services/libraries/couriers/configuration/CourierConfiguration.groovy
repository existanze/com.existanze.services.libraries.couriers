package com.existanze.services.libraries.couriers.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.web.client.RestTemplateAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.web.client.RestTemplate


@Configuration
@ConfigurationProperties(prefix = 'application')
class CourierConfiguration {

    @Value('${application.services.courier}')
    String courierUrl

    Map<String, ?> couriers


    // See build.gradle
    @Bean
    @ConditionalOnMissingBean(RestTemplate.class)
    @Primary
    public RestTemplate CourierRestTemplate(RestTemplateAutoConfiguration restTemplateAutoConfiguration) {
        return restTemplateAutoConfiguration.restTemplateBuilder().build();
    }

    @Bean
    Map courierCodes(){

        def courierMap = [:]

        couriers.each {
            courier ->
                courierMap.put(courier.value.name,courier.value.code)
        }

        return courierMap

    }

    @Bean
    public Map<String,String> availablePrinters(){

        Map printers = [:]

        couriers.each{
            k, v ->
                if(v.containsKey('printer')) {
                    printers.put(v['code'], v['printer'])
                }
        }

        return printers

    }
}
