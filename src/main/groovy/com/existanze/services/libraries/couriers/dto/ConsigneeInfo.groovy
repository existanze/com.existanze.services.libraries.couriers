package com.existanze.services.libraries.couriers.dto

class ConsigneeInfo extends PartyInfo {
    ConsigneeInfo(def orderReport){
        code = orderReport.partnerId
        // god knows what this does
        name = [orderReport.extraAttribute1, orderReport.extraAttribute2].findAll{it}?.join(' ')
        area = orderReport.systemAttribute6
        city = orderReport.systemAttribute5
        address = orderReport.systemAttribute4
        phone = orderReport.systemAttribute8
        phone2 = orderReport.extraAttribute3
        zip = orderReport.systemAttribute7
        email = orderReport.systemAttribute10
        country = orderReport.systemAttribute3

    }
}
