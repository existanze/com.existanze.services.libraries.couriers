package com.existanze.services.libraries.couriers.dto

import com.existanze.services.libraries.couriers.configuration.CourierConfiguration
import org.springframework.beans.factory.annotation.Autowired


class RequestorInfo extends PartyInfo{

    @Autowired
    CourierConfiguration courierConfiguration

    String courierCode

    RequestorInfo(String courierCode){
        code = '101'.equals(courierCode) ? courierConfiguration.couriers.courier_center.requestor_code : "Logika"
        name = "Logika"
        address = "ΘΕΣΗ ΠΛΑΚΩΤΟ - ΠΑΡΟΔΟΣ Λ. Γ. ΓΕΝΝΗΜΑΤΑ"
        area = "Μαγούλα"
        city = "Μαγούλα"
        phone = "2105552555"
        zip = "19018"
        email = "info@logika.gr"
    }
}
