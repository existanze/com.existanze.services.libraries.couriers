package com.existanze.services.libraries.couriers.dto

class PartyInfo {
    String code
    String name
    String city
    String area
    String address
    String country
    String phone
    String phone2
    String zip
    String email
}
