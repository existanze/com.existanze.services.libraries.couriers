package com.existanze.services.libraries.couriers.dto

class PaymentInfo {
    PaymentInfo(def orderReport){
       method = Double.valueOf(orderReport.systemAttribute12) == 0 ? 'Prepaid' : 'COD'
       amount = Double.valueOf(orderReport.systemAttribute12)
    }
    String method
    String amount
}
