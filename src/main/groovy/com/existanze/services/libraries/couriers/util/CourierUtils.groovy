package com.existanze.services.libraries.couriers.util

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.client.HttpStatusCodeException

class CourierUtils {

    private static final Logger logger = LoggerFactory.getLogger(CourierUtils.class)

    private static final ObjectMapper mapper = new ObjectMapper()

    static def extractErrors(HttpStatusCodeException httpException){

        def response = httpException.getResponseBodyAsString()

        try{
            return ['status': httpException.getRawStatusCode(),
                    'errors': mapper.readValue(response,Map.class)
            ]

        }catch(Exception e){
            logger.warn("Couldn't parse exception: {}: {}, with response body: {},",httpException.getRawStatusCode(),httpException.getMessage(),httpException.getResponseBodyAsString(),e)
            return ['status':httpException.getRawStatusCode(),
                    'errors': [
                            'error_message': httpException.getMessage()
                    ]
            ]
        }

    }
}
