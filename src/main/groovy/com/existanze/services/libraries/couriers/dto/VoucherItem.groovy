package com.existanze.services.libraries.couriers.dto

class VoucherItem {

   VoucherItem(){
      // default constructor provides dummy values parameterized constructor currently
      // not utilized as order reporting doesn't return item data
      description = "Package-1"
      weight = 1
      value = 1
   }

   VoucherItem(Map<String, ?> itemInfo){
      description = itemInfo['description']
      weight = itemInfo['weight']
      value = itemInfo['value']
   }
   String description
   String weight
   String value
}
