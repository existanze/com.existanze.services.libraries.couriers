package com.existanze.services.libraries.couriers.dto

import com.existanze.logika.integration.core.configuration.DepositorConfiguration
import org.springframework.beans.factory.annotation.Autowired

class DepositorInfo {

    // There doesn't seem to be a way to autowire subclasses, so I can't inject the depositor-specific
    // depositorConfiguration bean. Using parent class as constructor parameter, instead.
    DepositorInfo(DepositorConfiguration depositorConfiguration){
        depositorName = depositorConfiguration.name
        depositorCode = depositorConfiguration.depositorCode
        voucherCallbackUrl = depositorConfiguration.client.voucherCallbackUrl
        pdfCallbackUrl = depositorConfiguration.client.pdfCallbackUrl
    }
    String depositorCode
    String depositorName
    String voucherCallbackUrl
    String pdfCallbackUrl

}
