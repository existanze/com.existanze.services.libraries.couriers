package com.existanze.services.libraries.couriers.dto

class OrderData {

    OrderData(def orderReport){
       notes = orderReport.notes

        this.orderType = 'Forward'
        this.serviceType =  'Standard'
        this.totalValue = Double.valueOf(orderReport.systemAttribute12)
        this.totalQuantity = 1
        this.totalWeight = 1

       items = new ArrayList<VoucherItem>()
       items.add(new VoucherItem())
       //currently order reporting does not provide item info; might happen in the future, since new courier implementations
       // must support parsing actual item lists provided by the depositor service
       // For when items start landing in reports, there's a VoucherItem(Map) constructor to be used like so:
            // orderReport.items.each{
            //     itemInfo ->
            //         items.add(new VoucherItem(itemInfo))
            // }
    }
    String notes
    String orderType
    String serviceType
    int totalValue
    int totalQuantity
    int totalWeight
    List<VoucherItem> items
}
